return function()
	require("keys.cleanup")()

	-- Formatting
	Map({ "n", "v" }, "<leader>l", function()
		LazyVim.format({ force = true })
	end, { desc = "Format" })
	LazyVim.format.snacks_toggle():map("<leader>L")

	-- Toggle relativenumber
	Snacks.toggle.option("relativenumber", { name = "Relative Number" }):map("<leader>n")

	Map({ "n", "x" }, "<C-g>", function()
		Snacks.gitbrowse({
			open = function(url)
				vim.fn.setreg("+", url)
				print("Copied git link")
			end,
			notify = false,
		})
	end, { desc = "Yank git link" })
end
