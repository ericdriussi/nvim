return {
	{
		"<C-up>",
		function()
			require("multicursor-nvim").lineAddCursor(-1)
		end,
		mode = { "n", "v" },
	},
	{
		"<C-down>",
		function()
			require("multicursor-nvim").lineAddCursor(1)
		end,
		mode = { "n", "v" },
	},
	{
		"<C-n>",
		function()
			require("multicursor-nvim").matchAddCursor(1)
		end,
		mode = { "n", "v" },
	},
	{
		"<C-S-n>",
		function()
			require("multicursor-nvim").matchSkipCursor(-1)
		end,
		mode = { "n", "v" },
	},
	{
		"<esc>",
		function()
			vim.cmd("noh")
			require("multicursor-nvim").clearCursors()
		end,
		mode = { "n" },
	},
}
