local es_spellcheck = function()
	local bufnr = vim.api.nvim_get_current_buf()
	local first_line = vim.api.nvim_buf_get_lines(bufnr, 0, 1, false)[1]

	local latex_es_comment = "<!-- LTeX: language=es -->"
	if first_line == latex_es_comment then
		vim.api.nvim_buf_set_lines(bufnr, 0, 1, false, {})
	else
		vim.api.nvim_buf_set_lines(bufnr, 0, 1, false, { latex_es_comment, first_line })
	end
end

local function toggle_checkbox()
	local line = vim.api.nvim_get_current_line()
	local line_has_checkbox = string.match(line, "^%s*- %[[%sXx]%]")

	if line_has_checkbox then
		local is_checked = string.match(line, "%[[xX]%]")
		if is_checked then
			local new_line = string.gsub(line, "%[[xX]%]", "[ ]", 1)
			vim.api.nvim_set_current_line(new_line)
		else
			local new_line = string.gsub(line, "%[%s%]", "[x]", 1)
			vim.api.nvim_set_current_line(new_line)
		end
	end
end

return function()
	return {
		{ "<leader>m", "", desc = "+markdown", mode = { "n", "v" }, ft = "markdown" },

		{
			"<leader>ms",
			es_spellcheck,
			desc = "Toggle Spanish spellcheck",
			mode = "n",
			ft = "markdown",
		},

		{
			"<leader>mx",
			toggle_checkbox,
			desc = "Toggle checkbox",
			ft = "markdown",
		},
	}
end
