return {
	keys = function()
		return {
			{ "<leader>f", LazyVim.pick("files"), desc = "Find File" },
			{ "<leader>F", LazyVim.pick("live_grep"), desc = "Find in Files" },
			{ "<leader>F", LazyVim.pick("grep_visual"), mode = "v", desc = "Find Selection" },

			{ "gr", "<cmd>FzfLua lsp_references<cr>", desc = "References" },
			{ "gd", "<cmd>FzfLua lsp_definitions<cr>", desc = "Goto Definition" },
			{ "gi", "<cmd>FzfLua lsp_implementations<cr>", desc = "Goto Implementation" },
			{ "gE", "<cmd>FzfLua diagnostics_document<cr>", desc = "Goto (ALL) Errors" },

			{ "<C-Space>", "<cmd>FzfLua spell_suggest<cr>", "Spelling" },

			{ "<leader>A", "<cmd>FzfLua commands<cr>", desc = "Actions" },
			{ "<leader>H", "<cmd>FzfLua help_tags<cr>", desc = "Help" },
			{ "<leader>R", "<cmd>FzfLua registers<cr>", desc = "Registers" },
			{ "<leader>K", "<cmd>FzfLua keymaps<cr>", desc = "Keymaps" },
		}
	end,
}
