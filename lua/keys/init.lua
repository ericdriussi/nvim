Map = vim.keymap.set
Nmap = function(lhs, rhs, opts)
	Map("n", lhs, rhs, opts or {})
end

return {
	lazyvim = require("keys.lazyvim"),
	nvim = require("keys.nvim"),

	fzf = require("keys.fzf"),
	gitsigns = require("keys.gitsigns"),
	diffview = require("keys.diffview"),
	neo_tree = require("keys.neo_tree"),
	custom_md = require("keys.custom_md"),
	markdowny = require("keys.markdowny"),
	multicursor = require("keys.multi"),

	markdown_preview = {
		{
			"<leader>mm",
			"<cmd>MarkdownPreviewToggle<cr>",
			desc = "Markdown Preview",
			ft = "markdown",
		},
	},

	mason = { { "<leader>?", "<cmd>Mason<cr>", desc = "Mason" } },

	gx = { { "gx", "<cmd>Browse<cr>", desc = "Open in browser" } },

	comment = {
		{ "<leader>/", mode = { "n" }, "<cmd>CommentToggle<cr>", desc = "Toggle Comment" },
		{ "<leader>/", mode = { "x" }, ":'<,'>CommentToggle<cr>", silent = true, desc = "Toggle Comment" },
	},

	lsp = {
		{
			"rN",
			function()
				Snacks.rename.rename_file()
			end,
		},
		{ "rn", vim.lsp.buf.rename },
		{ "<c-k>", vim.lsp.buf.signature_help },
		{ "<M-cr>", LazyVim.lsp.action.source },
	},

	cmp = function()
		return {
			preset = "enter",
			["<Tab>"] = {
				function(cmp)
					local copilot_ok, copilot_suggestion = pcall(require, "copilot.suggestion")
					if cmp.snippet_active() then
						return cmp.accept()
					elseif copilot_ok and copilot_suggestion.is_visible() then
						copilot_suggestion.accept()
						return true
					else
						return cmp.select_and_accept()
					end
				end,
				"snippet_forward",
				"fallback",
			},
		}
	end,

	grug = function()
		return {
			{
				"<leader>S",
				function()
					local ext = vim.bo.buftype == "" and vim.fn.expand("%:e")
					require("grug-far").open({
						transient = true,
						prefills = {
							filesFilter = ext and ext ~= "" and "*." .. ext or nil,
						},
					})
				end,
				mode = { "n", "v" },
				desc = "Seek & Destroy",
			},
		}
	end,

	none = function()
		return {}
	end,
}
