return {
	{
		"<leader>mb",
		":lua require('markdowny').bold()<cr>",
		desc = "Make bold",
		mode = "v",
		ft = "markdown",
	},
	{
		"<leader>mi",
		":lua require('markdowny').italic()<cr>",
		desc = "Make italics",
		mode = "v",
		ft = "markdown",
	},
	{
		"<leader>ml",
		":lua require('markdowny').link()<cr>",
		desc = "Make link",
		mode = "v",
		ft = "markdown",
	},
	{
		"<leader>mc",
		":lua require('markdowny').code()<cr>",
		desc = "Make inline code",
		mode = "v",
		ft = "markdown",
	},

	{ "<leader>mb", "viw:lua require('markdowny').bold()<cr>", desc = "Make bold", ft = "markdown" },
	{ "<leader>mi", "viw:lua require('markdowny').italic()<cr>", desc = "Make italics", ft = "markdown" },
	{ "<leader>ml", "viw:lua require('markdowny').link()<cr>", desc = "Make link", ft = "markdown" },
	{ "<leader>mc", "viw:lua require('markdowny').code()<cr>", desc = "Make inline code", ft = "markdown" },
}
