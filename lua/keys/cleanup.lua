local function rm_by_prefix(mode, prefix, just_childs)
	local keymaps = vim.api.nvim_get_keymap(mode)

	for _, keymap in ipairs(keymaps) do
		local lhs = keymap.lhs
		local is_child_mapping = #lhs > #prefix
		local starts_with_prefix = vim.startswith(lhs, prefix)

		if starts_with_prefix and (not just_childs or is_child_mapping) then
			vim.keymap.del(mode, lhs)
		end
	end
end

local function rm_list_by_prefix(mode, prefix_list, just_childs)
	for _, prefix in ipairs(prefix_list) do
		rm_by_prefix(mode, prefix, just_childs)
	end
end

local function rm_list(mode, lhs_list)
	for _, lhs in ipairs(lhs_list) do
		vim.keymap.del(mode, lhs)
	end
end

return function()
	local leader = vim.g.mapleader
	rm_by_prefix("v", leader .. "c")
	rm_by_prefix("n", leader .. "f", true)

	rm_list_by_prefix("n", {
		leader .. "x",
		leader .. "q",
		leader .. "w",
		leader .. "b",
		leader .. "c",
		leader .. "sn",
		leader .. "<Tab>",
	})

	rm_list("n", {
		"<leader>-",
		"<leader>|",
		"<leader>`",
		"<leader>gG",
		"<leader>gL",
		"<leader>gf",
		"<leader>gg",
		"g[",
		"g]",
		"g%",
		"<C-h>",
		"<C-j>",
		"<C-k>",
		"<C-l>",
	})
end
