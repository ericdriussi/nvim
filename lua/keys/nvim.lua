local function vmap(lhs, rhs)
	Map("v", lhs, rhs)
end
local function imap(lhs, rhs)
	Map("i", lhs, rhs)
end

return function()
	Nmap("L", "$")
	Nmap("H", "^")
	vmap("L", "$")
	vmap("H", "^")

	-- Center half-page jumps and add them to jumplist
	Nmap("<C-d>", "<C-d>zzm'")
	Nmap("<C-u>", "<C-u>zzm'")

	-- Sensible copy-pasting to and from system clipboard
	vmap("<C-y>", "\"+y <bar> <cmd>echom 'Yanked to system!'<cr> gv<esc>")
	Nmap("<C-y>", "viw\"+y <bar> <cmd>echom 'Yanked to system!'<cr> gv<esc>")
	vmap("<C-p>", '"+p <cr> gv<esc>')
	Nmap("<C-p>", "\"+p:let @/ = ''<cr>")
	imap("<C-p>", "<esc>\"+p:let @/ = ''<cr>")

	-- Y like you C
	Nmap("Y", "y$")

	-- Prevent cursor from jumping back
	vmap("y", "ygv<ESC>")

	-- Paste over selected text without screwing the reg
	vmap("p", '"_dP')
	vmap("P", '"_dP')

	-- Select all
	Nmap("<C-a>", "ggVG")

	-- Center search selection
	Nmap("n", "nzzzv")
	Nmap("N", "Nzzzv")

	-- Search selected text
	vmap("//", "y/\\V<C-R>=escape(@\",'/')<cr><cr>")

	-- Dumb-replace word
	Nmap("rp", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

	-- Close
	Nmap("<C-w>", "<cmd>q!<cr>")

	-- Save all
	Map({ "i", "x", "n", "s" }, "<C-s>", "<cmd>silent wa<cr><esc>", { desc = "Save Files" })

	-- Move Lines
	Nmap("<M-h>", "<<")
	Nmap("<M-l>", ">>")
	vmap("<M-h>", "<gv")
	vmap("<M-l>", ">gv")

	-- Resize vertical splits
	Nmap("+", "<cmd>vertical resize +5<cr>")
	Nmap("-", "<cmd>vertical resize -5<cr>")

	-- Resize horizontal splits
	Nmap("<M-+>", "<cmd>resize +2<cr>")
	Nmap("<M-->", "<cmd>resize -2<cr>")

	-- Navigate splits
	Nmap("<C-M-l>", "<cmd>wincmd l<cr>")
	Nmap("<C-M-h>", "<cmd>wincmd h<cr>")
	Nmap("<C-M-j>", "<cmd>wincmd j<cr>")
	Nmap("<C-M-k>", "<cmd>wincmd k<cr>")

	-- Add number jumps to jumplist
	Nmap("<expr> k", '(v:count > 5 ? "m\'" . v:count : "") . \'k\'')
	Nmap("<expr> j", '(v:count > 5 ? "m\'" . v:count : "") . \'j\'')

	-- Increment/Decrement number
	Nmap("<M-a>", "<C-a>")
	Nmap("<M-x>", "<C-x>")

	-- Only yank deleted lines if not empty
	Nmap("dd", function()
		if vim.api.nvim_get_current_line():match("^%s*$") then
			return '"_dd'
		end
		return "dd"
	end, { expr = true })

	-- Goto Diagnostics
	Nmap("ge", function()
		vim.diagnostic.open_float({ scope = "line" })
	end, { desc = "Get Error" })
	Nmap("gn", function()
		vim.diagnostic.goto_next()
	end, { desc = "Goto Next Error" })
	Nmap("gp", function()
		vim.diagnostic.goto_prev()
	end, { desc = "Goto Prev Error" })

	-- Folds
	Nmap("zo", "zo", { desc = "Open fold" })
	Nmap("zO", "zR", { desc = "Open all folds" })
	Nmap("zc", "zc", { desc = "Close fold" })
	Nmap("zC", "zM", { desc = "Close all folds" })

	-- Create splits
	Nmap("<leader>sv", "<cmd>vsplit<cr>", { desc = "Split Right" })
	Nmap("<leader>sh", "<cmd>split<cr>", { desc = "Split Down" })

	-- Help with merge conflicts
	Map({ "v", "n" }, "<leader>gh", "<cmd>diffget LOCAL<cr>", { desc = "Get Current (Local)" })
	Map({ "v", "n" }, "<leader>gl", "<cmd>diffget REMOTE<cr>", { desc = "Get Incoming (Remote)" })

	-- Format Json and Columns
	Nmap("<leader>pj", "<cmd>%!jq<cr>", { desc = "Json" })
	Nmap("<leader>pc", "<cmd>%!column -t<cr>", { desc = "Columns" })

	-- Terminal
	Map("t", "<Esc>", function()
		vim.api.nvim_input("<C-\\><C-n>")
	end, { desc = "Normal mode in terminal" })
	Map("t", "<C-w>", function()
		vim.cmd.bd({ bang = true })
	end, { desc = "Close terminal" })
	Nmap("<Leader>tt", function()
		vim.cmd.tabnew()
		vim.cmd.term()
		vim.cmd.startinsert()
	end, { desc = "Terminal Tab" })
	Nmap("<Leader>th", function()
		vim.cmd.split()
		vim.cmd.term()
		vim.cmd.startinsert()
	end, { desc = "Terminal Horizontal" })
	Nmap("<Leader>tv", function()
		vim.cmd.vsplit()
		vim.cmd.term()
		vim.cmd.startinsert()
	end, { desc = "Terminal Vertical" })

	-- Quick file diff
	local is_diff = false
	Nmap("<leader>d", function()
		local function toggleOff()
			vim.cmd("windo diffoff")
			vim.api.nvim_set_option_value("cursorbind", false, {})
			vim.api.nvim_set_option_value("scrollbind", false, {})
			is_diff = false
		end

		local function toggleOn()
			vim.cmd("windo diffthis")
			vim.cmd("windo set wrap")
			vim.api.nvim_set_option_value("cursorbind", true, {})
			vim.api.nvim_set_option_value("scrollbind", true, {})
			is_diff = true
		end

		if is_diff then
			toggleOff()
		else
			toggleOn()
		end
	end, { desc = "Diff files" })

	-- Yank diagnostics
	Nmap("yd", function()
		local row, col = unpack(vim.api.nvim_win_get_cursor(0))
		local diagnostics_in_line = vim.diagnostic.get(0, { lnum = row - 1 })

		if not diagnostics_in_line or next(diagnostics_in_line) == 0 then
			return
		end

		local diganostics_under_cursor = vim.tbl_filter(function(diagnostic)
			return diagnostic.col == col
		end, diagnostics_in_line)

		vim.fn.setreg(
			"+",
			vim.tbl_map(function(diagnostic)
				return diagnostic.message
			end, diganostics_under_cursor)
		)
		print("Yanked diagnostics")
	end, { desc = "Yank Diagnostics" })
end
