local function merge_hl(from, into)
	vim.api.nvim_set_hl(0, into, {
		fg = from.fg,
		bg = from.bg,
		bold = from.bold,
		italic = from.italic,
		underline = from.underline,
		undercurl = from.undercurl,
		reverse = from.reverse,
		standout = from.standout,
		strikethrough = from.strikethrough,
		sp = from.sp,
	})
end

return {
	{
		"nvim-neo-tree/neo-tree.nvim",
		keys = require("keys").neo_tree.keys,

		opts = {
			close_if_last_window = true,
			window = {
				width = 30,
				mappings = require("keys").neo_tree.mappings,
			},

			event_handlers = {
				{
					event = "after_render",
					handler = function()
						local hl_normal = vim.api.nvim_get_hl(0, { name = "Normal" })
						merge_hl(hl_normal, "NeoTreeNormal")
						merge_hl(hl_normal, "NeoTreeEndOfBuffer")
					end,
				},
			},

			filesystem = {
				filtered_items = {
					hide_dotfiles = false,
				},

				components = {
					-- project name as root dir: https://github.com/nvim-neo-tree/neo-tree.nvim/discussions/681#discussioncomment-5429393
					name = function(config, node, state)
						local result = require("neo-tree.sources.filesystem.components").name(config, node, state)
						if node:get_depth() == 1 and node.type ~= "message" then
							result.text = vim.fn.fnamemodify(node.path, ":t")
						end
						return result
					end,
				},
			},

			-- already using colors, why symbols?
			default_component_configs = {
				git_status = {
					symbols = {
						untracked = "",
						ignored = "",
						unstaged = "",
						staged = "",
						conflict = "",
					},
				},
			},
		},
	},

	{
		-- Open links in browser
		"chrishrb/gx.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
		cmd = { "Browse" },
		keys = require("keys").gx,
		opts = {
			handler_options = {
				search_engine = "duckduckgo",
			},
		},
	},

	{
		"ibhagwan/fzf-lua",
		keys = require("keys").fzf.keys,

		opts = {
			actions = {
				files = {
					true,
					["ctrl-h"] = require("fzf-lua.actions").file_split,
				},
			},

			keymap = {
				fzf = {
					["ctrl-a"] = "select-all",
				},
			},

			files = {
				header = false,
				git_icons = false,
			},
			grep = {
				header = false,
				resume = true,
			},
			spell_suggest = {
				winopts = {
					backdrop = 100,
					height = 0.4,
					width = 0.3,
				},
			},
		},
	},
}
