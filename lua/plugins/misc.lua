local colorizer_ft = { "css", "scss", "html", "javascript", "typescript", "typescriptreact", "astro" }

return {
	{ "EricDriussi/remember-me.nvim", config = true },

	{
		"norcalli/nvim-colorizer.lua",
		ft = colorizer_ft,
		opts = colorizer_ft,
	},

	{
		"MagicDuck/grug-far.nvim",
		keys = require("keys").grug,
	},

	{
		"oysandvik94/curl.nvim",
		cmd = { "CurlOpen" },
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		opts = {
			mappings = {
				execute_curl = "<tab>",
			},
		},
	},

	{
		"andymass/vim-matchup",
		config = function()
			vim.api.nvim_set_hl(0, "MatchWord", {})
			vim.api.nvim_set_hl(0, "MatchWordCur", {})
			vim.api.nvim_set_hl(0, "MatchParenCur", {})
			vim.api.nvim_set_hl(0, "MatchParen", {})
		end,
	},

	{
		"folke/which-key.nvim",
		keys = require("keys").custom_md,
		opts = function(_, opts)
			opts.preset = "classic"
			opts.icons = { mappings = false }
			opts.plugins = {
				marks = false,
				registers = false,

				-- Help for vim stuff
				presets = {
					operators = false,
					motions = false,
					text_objects = false,
					windows = false,
					nav = false,
					z = false,
					g = false,
				},
			}

			-- No popup entering V mode
			opts.triggers = {
				{ "<auto>", mode = "nistc" },
				{ "<leader>", mode = { "n", "v" } },
			}

			opts.spec = {
				{ "<leader>s", group = "split" },
				{ "<leader>p", group = "pretty" },
				{ "<leader>t", group = "terminal" },
				{
					mode = { "n", "v" },
					{ "<leader>P", desc = "Print" },
					{ "<leader>g", group = "git" },
					{ "<leader>c", hidden = true },
				},
				{ "<leader>u", hidden = true },
				{ "<leader>w", hidden = true },
				{ "gc", hidden = true },
			}
		end,
	},
}
