local prettier = { "prettierd", "prettier", stop_after_first = true }

return {
	"mfussenegger/nvim-lint",
	opts = {
		linters_by_ft = {
			markdown = { "markdownlint-cli2" },
			yml = { "ansible_lint" },
			yaml = { "ansible_lint" },
			zsh = { "zsh" },
		},
	},

	{
		"stevearc/conform.nvim",
		opts = {
			formatters_by_ft = {
				sh = { "shfmt", "shellharden" },
				zsh = { "shfmt", "shellharden" },

				yaml = { "yamlfmt" },
				toml = { "taplo" },

				markdown = { "markdownlint-cli2" },

				astro = prettier,

				javascript = prettier,
				typescript = prettier,
				javascriptreact = prettier,
				typescriptreact = prettier,
			},

			formatters = {
				yamlfmt = {
					prepend_args = { "-formatter", "retain_line_breaks=true,scan_folded_as_literal=true" },
				},

				taplo = {
					inherit = false,
					command = "taplo",
					args = {
						"format",
						"--option",
						"column_width=120",
						"--option",
						"inline_table_expand=false",
						"--option",
						"indent_string=	",
						"-",
					},
				},
			},
		},
	},
}
