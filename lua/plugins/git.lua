return {
	{
		"sindrets/diffview.nvim",
		cmd = { "DiffviewOpen", "DiffviewFileHistory" },
		keys = require("keys").diffview.keys,
		opts = function()
			return {
				keymaps = require("keys").diffview.keymaps(require("diffview.actions")),
				file_history_panel = {
					win_config = {
						position = "bottom",
						height = 10,
					},
				},
			}
		end,
	},

	{
		"lewis6991/gitsigns.nvim",
		opts = {
			signs = {
				delete = { text = "▎" },
				topdelete = { text = "▎" },
			},
			on_attach = function(buffer)
				require("keys").gitsigns(buffer)
			end,
		},
	},
}
