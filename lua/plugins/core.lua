return {
	{
		"LazyVim/LazyVim",
		opts = {
			news = { lazyvim = false, neovim = false },
			colorscheme = "gruvbox-material",
		},
	},

	-- disabled
	{ "catppuccin/nvim", enabled = false },
	{ "ellisonleao/gruvbox", enabled = false },
	{ "folke/flash.nvim", enabled = false },
	{ "folke/persistence.nvim", enabled = false },
	{ "folke/todo-comments.nvim", enabled = false },
	{ "folke/tokyonight.nvim", enabled = false },
	{ "folke/trouble.nvim", enabled = false },
	{ "folke/ts-comments.nvim", enabled = false },

	{
		"folke/snacks.nvim",
		opts = {
			notifier = { enabled = false },
			dashboard = { enabled = false },
			lazygit = { enabled = false },
			terminal = { enabled = false },
			scroll = { enabled = false },
			indent = {
				filter = function(buf)
					return vim.bo[buf].filetype ~= "markdown" and vim.bo[buf].buftype ~= "nofile"
				end,
			},
		},
		keys = require("keys").none,
	},
}
