return {
	{
		"zbirenbaum/copilot.lua",
		event = "InsertEnter",
		keys = {
			{
				"<Right>",
				function()
					local copilot_ok, copilot_suggestion = pcall(require, "copilot.suggestion")
					if copilot_ok and copilot_suggestion.is_visible() then
						copilot_suggestion.accept()
					else
						vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes("<Right>", true, false, true), "n", true)
					end
				end,
				mode = "i",
			},
		},
		opts = {
			panel = { enalbed = false },
			suggestion = {
				auto_trigger = true,
			},
		},
	},
}
