vim.api.nvim_create_autocmd("ColorScheme", {
	group = vim.api.nvim_create_augroup("custom_highlights_gruvboxmaterial", {}),
	pattern = "gruvbox-material",
	command = "hi NormalFloat guibg=#32302f",
})

return {
	"sainnhe/gruvbox-material",
	config = function()
		vim.g.gruvbox_material_foreground = "mix"
		vim.g.gruvbox_material_better_performance = 1
		vim.g.gruvbox_material_enable_italic = 1
		vim.g.gruvbox_material_current_word = "bold,italic"
	end,
}
