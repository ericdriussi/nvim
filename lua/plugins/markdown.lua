return {
	{
		"antonk52/markdowny.nvim",
		ft = "markdown",
		keys = require("keys").markdowny,
	},

	{
		"iamcco/markdown-preview.nvim",
		ft = "markdown",
		build = "cd app && npm install",
		keys = require("keys").markdown_preview,
	},

	{
		"MeanderingProgrammer/render-markdown.nvim",
		config = true,
	},
}
