return {
	{
		"jinh0/eyeliner.nvim",
		opts = {
			highlight_on_key = true,
			dim = true,
			max_length = 300,
		},
	},

	{
		"jake-stewart/multicursor.nvim",
		branch = "1.0",
		config = true,
		keys = require("keys").multicursor,
	},

	{
		"terrortylor/nvim-comment",
		keys = require("keys").comment,
		cmd = "CommentToggle",
		dependencies = {
			{
				"JoosepAlviste/nvim-ts-context-commentstring",
				config = function()
					require("ts_context_commentstring").setup({
						enable_autocmd = false,
					})
				end,
			},
		},
		config = function()
			require("nvim_comment").setup({
				create_mappings = false,
				comment_empty = false,
				hook = function()
					require("ts_context_commentstring").update_commentstring()
				end,
			})
		end,
	},

	{ "chaoren/vim-wordmotion" }, -- Handle camelCase and snek_case

	{
		"tpope/vim-surround",
		init = function()
			local map = function(lhs, rhs)
				return vim.api.nvim_set_keymap("v", lhs, rhs, {})
			end

			map("(", "S)")
			map("[", "S]")
			map("{", "S}")
			map("'", "S'")
			map('"', 'S"')
			map("`", "S`")
		end,
	},

	{
		"andrewferrier/debugprint.nvim",
		keys = {
			{ "<leader>P", mode = "n" },
			{ "<leader>P", mode = "x" },
		},
		cmd = {
			"ToggleCommentDebugPrints",
			"DeleteDebugPrints",
		},

		opts = {
			display_counter = false,
			print_tag = "=== DEBUG ===",
			keymaps = {
				normal = {
					plain_below = nil,
					plain_above = nil,
					variable_below = "<leader>P",
					variable_above = nil,
					variable_below_alwaysprompt = nil,
					variable_above_alwaysprompt = nil,
					textobj_below = nil,
					textobj_above = nil,
					toggle_comment_debug_prints = nil,
					delete_debug_prints = nil,
				},
				visual = {
					variable_below = "<leader>P",
					variable_above = nil,
				},
			},
		},
	},
}
