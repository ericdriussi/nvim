return {
	{
		"neovim/nvim-lspconfig",
		opts = function(_, opts)
			local keys = require("lazyvim.plugins.lsp.keymaps").get()
			-- unset lazyvim lsp keymaps
			for i, key in ipairs(keys) do
				if vim.startswith(key[1], "<leader>c") then
					keys[i][2] = false
				end
			end
			keys[#keys + 1] = { "gK", false }
			keys[#keys + 1] = { "gy", false }
			keys[#keys + 1] = { "gI", false }

			-- set custom lsp keymaps
			for _, key in ipairs(require("keys").lsp) do
				keys[#keys + 1] = key
			end

			opts.inlay_hints = { enabled = false }
			opts.diagnostics.virtual_text.prefix = " "
			opts.diagnostics.virtual_text.severity = { min = vim.diagnostic.severity.ERROR }

			opts.diagnostics.float = {
				focusable = false,
				source = "if_many",
				header = "",
				prefix = function(diagnostic)
					local icons = LazyVim.config.icons.diagnostics
					local spacing = " "
					local severity = diagnostic.severity

					if severity == vim.diagnostic.severity.ERROR then
						return icons.Error .. spacing, "DiagnosticError"
					elseif severity == vim.diagnostic.severity.WARN then
						return icons.Warn .. spacing, "DiagnosticWarn"
					elseif severity == vim.diagnostic.severity.INFO then
						return icons.Info .. spacing, "DiagnosticInfo"
					else
						return icons.Hint .. spacing, "DiagnosticHint"
					end
				end,
			}

			opts.servers.ansiblels = { filetypes = { "yaml", "yml" } }
			opts.servers.ltex = {
				settings = {
					ltex = {
						checkFrequency = "save",
					},
				},
			}
		end,
	},

	{
		"antosha417/nvim-lsp-file-operations",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-neo-tree/neo-tree.nvim",
		},
		config = function()
			require("lsp-file-operations").setup()
		end,
	},

	{
		"williamboman/mason.nvim",
		keys = require("keys").mason,
		opts = {
			ui = {
				keymaps = {
					uninstall_package = "d",
				},
				max_concurrent_installers = 6,
			},
			ensure_installed = {
				"actionlint",
				"bash-language-server",
				"beautysh",
				"css-lsp",
				"docker-compose-language-service",
				"dockerfile-language-server",
				"emmet-language-server",
				"eslint-lsp",
				"eslint_d",
				"gofumpt",
				"gopls",
				"html-lsp",
				"json-lsp",
				"ltex-ls",
				"lua-language-server",
				"markdownlint",
				"marksman",
				"mypy",
				"python-lsp-server",
				"prettierd",
				"shellcheck",
				"shfmt",
				"stylua",
				"typescript-language-server",
				"yaml-language-server",
				"yamlfmt",
				"yamllint",
			},
		},
	},

	{
		"saghen/blink.cmp",
		dependencies = {
			{
				"kristijanhusak/vim-dadbod-completion",
				"moyiz/blink-emoji.nvim",
			},
		},

		opts = {
			keymap = require("keys").cmp(),
			sources = {
				default = { "emoji", "dadbod" },

				providers = {
					emoji = { name = "Emoji", module = "blink-emoji" },
					dadbod = { name = "Dadbod", module = "vim_dadbod_completion.blink" },
				},
			},

			completion = {
				ghost_text = {
					enabled = false,
				},
			},
		},
	},

	{
		"nvim-treesitter/nvim-treesitter",
		keys = require("keys").none,
		opts = {
			incremental_selection = {
				enable = true,
				keymaps = {
					init_selection = "<Enter>",
					node_incremental = "<Enter>",
					node_decremental = "<BS>",
				},
			},

			matchup = {
				enable = true,
			},

			textobjects = {
				select = {
					enable = true,
					keymaps = {
						["am"] = "@function.outer",
						["im"] = "@function.inner",
						["aa"] = "@parameter.outer",
						["ia"] = "@parameter.inner",
						["ac"] = "@call.outer",
						["ic"] = "@call.inner",
						["al"] = "@loop.outer",
						["il"] = "@loop.inner",
						["ai"] = "@conditional.outer",
						["ii"] = "@conditional.inner",
					},
				},
				move = {
					enable = true,
					set_jumps = true, -- register in jump list
					goto_next_start = {
						["m"] = "@function.outer",
					},
					goto_previous_start = {
						["M"] = "@function.outer",
					},
				},
			},
		},

		init = function()
			require("vim.treesitter.query").add_predicate("is-mise?", function(_, _, bufnr, _)
				local filepath = vim.api.nvim_buf_get_name(tonumber(bufnr) or 0)
				local filename = vim.fn.fnamemodify(filepath, ":t")
				return string.match(filename, ".*mise.*%.toml$")
			end, { force = true, all = false })
		end,
	},

	{
		"jmbuhr/otter.nvim",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
		},
		config = function()
			vim.api.nvim_create_autocmd({ "FileType" }, {
				pattern = { "toml" },
				group = vim.api.nvim_create_augroup("EmbedToml", {}),

				callback = function()
					require("otter").activate()
				end,
			})
		end,
	},
}
