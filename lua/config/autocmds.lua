local aucmd = vim.api.nvim_create_autocmd
local augroup = vim.api.nvim_create_augroup

aucmd("BufWinEnter", {
	desc = "Avoid autocomment on 'o' in commented line",
	command = "set formatoptions-=o",
})

aucmd("TextYankPost", {
	desc = "Highlight on yank",
	group = augroup("HighlightYank", {}),
	callback = function()
		vim.highlight.on_yank({
			higroup = "TermCursor",
			timeout = 60,
		})
	end,
})

aucmd("FileType", {
	desc = "GoTo navigation for help files",
	group = augroup("GoToHelpMan", {}),
	pattern = { "help", "man" },
	callback = function()
		vim.keymap.set("n", "gd", "<C-]>")
	end,
})

aucmd("TermOpen", {
	desc = "No numbers for terminal",
	group = augroup("Terminal", {}),
	callback = function()
		vim.opt.number = false
		vim.opt.relativenumber = false
		vim.opt.signcolumn = "no"
	end,
})
