vim.g.lazyvim_picker = "fzf"
vim.g.lazyvim_statuscolumn = {
	folds_open = true,
	folds_githl = true, -- color folds with gitsign color
}
vim.g.lazygit_config = false

vim.opt.clipboard = ""
vim.opt.conceallevel = 0
vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 5
vim.opt.spelllang = { "en", "es" }
vim.opt.spelloptions:append("camel")
vim.opt.undolevels = 10000
vim.opt.updatetime = 200
vim.opt.undodir = vim.env.HOME .. "/.config/nvim/.undodir/"
vim.opt.wrap = true
vim.opt.history = 1000
vim.opt.swapfile = false

vim.opt.title = true
local root_path = vim.fs.root(0, { ".git" }) or vim.fn.expand("%:p:h")
local root_dir_name = vim.fn.fnamemodify(root_path, ":t")
vim.opt.titlestring = "NVIM(" .. root_dir_name .. ") - %f"
