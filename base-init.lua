Map = vim.keymap.set
Nmap = function(lhs, rhs, opts)
	Map("n", lhs, rhs, opts or {})
end

require("config.options")
require("keys.nvim")()

-- Sane defaults
vim.opt.inccommand = "split"
vim.opt.cursorline = true
vim.opt.linebreak = true
vim.opt.breakindent = true
vim.opt.scrolloff = 8
vim.opt.sidescrolloff = 5
vim.opt.wrap = true
vim.opt.number = true
vim.opt.numberwidth = 3
vim.opt.relativenumber = true
vim.opt.signcolumn = "yes"
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.infercase = true
