Map = vim.keymap.set
Nmap = function(lhs, rhs, opts)
	Map("n", lhs, rhs, opts or {})
end

require("config.options")
require("keys.nvim")()

-- Perf settings
vim.opt.cursorline = false
vim.opt.cursorcolumn = false
vim.opt.relativenumber = false
vim.opt.signcolumn = "no"
vim.opt.hidden = false
vim.opt.syntax = "off"
vim.opt.lazyredraw = true

-- Disable builtin plugins
local disabled_built_ins = {
	"2html_plugin",
	"getscript",
	"getscriptPlugin",
	"gzip",
	"logipat",
	"netrw",
	"netrwPlugin",
	"netrwSettings",
	"netrwFileHandlers",
	"matchit",
	"tar",
	"tarPlugin",
	"rrhelper",
	"spellfile_plugin",
	"vimball",
	"vimballPlugin",
	"zip",
	"zipPlugin",
	"tutor",
	"rplugin",
	"synmenu",
	"optwin",
	"compiler",
	"bugreport",
	"ftplugin",
}

for _, plugin in pairs(disabled_built_ins) do
	vim.g["loaded_" .. plugin] = 1
end
